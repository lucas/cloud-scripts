#!/usr/bin/env ruby
# Author:: Sebastien Badia <seb@sebian.fr>, Lucas Nussbaum <lucas@debian.org>
# Date:: Wed Mar 21 01:18:02 +0100 2012
#

require 'fileutils'
require 'json'
require 'net/ssh'
require 'net/scp'
require 'net/ssh/multi'
require 'peach'
require 'pp'
require 'tempfile'
require 'optparse'

$nodes = nil

options = OptionParser::new do |opts|
  opts.banner = "Usage: ./setup-ganglia [options]"
  opts.separator ""
  opts.separator "Options:"
  opts.on("-n", "--nodes FILE", "Nodes file") do |n|
    $nodes=IO::readlines(n).map { |l| l.chomp }
  end
end
options.parse!(ARGV)

#############################
$startt = Time::now
MSG_ERROR=0       # Exit return codes
MSG_WARNING=1
MSG_INFO=2

def msg(str, type=nil, quit=false)
  case type
  when MSG_ERROR
    puts("### Error: #{str} ###")
  when MSG_WARNING
    puts("### Warning: #{str} ###")
  when MSG_INFO
    puts("[#{(Time.now - $startt).to_i}] #{str}")
  else
    puts str
  end
  exit 1 if quit
end # def:: msg

def open_channel(session, group = nil)
  if group.is_a?(Symbol)
    session.with(group).open_channel do |channel|
      yield(channel)
    end
  elsif group.is_a?(Array)
    session.on(*group).open_channel do |channel|
      yield(channel)
    end
  elsif group.is_a?(Net::SSH::Multi::Server)
    session.on(group).open_channel do |channel|
      yield(channel)
    end
  else
    session.open_channel do |channel|
      yield(channel)
    end
  end
end # def:: open_channel

def nexec(session, cmd, group = nil, critical = true, showerr = true, showout = true)
  outs = {}
  errs = {}
  channel = open_channel(session,group) do |chtmp|
    chtmp.exec(cmd) do |ch, success|
      unless success
        msg("unable to execute '#{cmd}' on #{ch.connection.host}",MSG_ERROR)
      end
        msg("Executing '#{cmd}' on #{ch.connection.host}]",MSG_INFO) \
        if showout
    end
  end
  channel.on_data do |chtmp,data|
    outs[chtmp.connection.host] = [] unless outs[chtmp.connection.host]
    outs[chtmp.connection.host] << data.strip
    msg("[#{chtmp.connection.host}] #{data.strip}") \
    if showout
  end
  channel.on_extended_data do |chtmp,type,data|
    errs[chtmp.connection.host] = [] unless errs[chtmp.connection.host]
    errs[chtmp.connection.host] << data.strip
    msg("[#{chtmp.connection.host} E] #{data.strip}") \
      if showout
  end

  channel.on_request("exit-status") do |chtmp, data|
    status = data.read_long
    if status != 0
      if showerr or critical
        msg("exec of '#{cmd}' on #{chtmp.connection.host} failed " \
            "with return status #{status.to_s}",MSG_ERROR)
        msg("---stdout dump---")
        outs[chtmp.connection.host].each { |out| msg(out) } if \
          outs[chtmp.connection.host]
        msg("---stderr dump---")
        errs[chtmp.connection.host].each { |err| msg(err) } if \
          errs[chtmp.connection.host]
        msg("---\n")
      end
      exit 1 if critical
    end
  end
  channel.wait
  return outs
end # def:: nexec

#############################

ssh = Net::SSH::Multi.start
$nodes.each do |node|
  ssh.use "root@#{node}"
end

nexec(ssh, "apt-get -y install ganglia-monitor")
ssh.loop

File::open('gmond.conf', 'w') do |f|
  conf = IO::read('examples/gmond.conf')
  ip = `ip addr show dev eth0`.grep(/inet /)[0].split[1].split('/')[0]
  conf.gsub!('SERVERADDR',ip)
  f.puts conf
end

$nodes.each do |node|
  Net::SCP.start(node, 'root') do |scp|
    scp.upload!('gmond.conf',"/etc/ganglia/")
  end
end


nexec(ssh, "/etc/init.d/ganglia-monitor restart")
ssh.loop

FileUtils::rm "gmond.conf"

