#!/usr/bin/ruby -w

require 'open-uri'
require 'pp'
require 'json'
require 'optparse'
require 'zlib'

EXCLUDED_PKGS = %w{
}

$modes = []
$dist = 'jessie'
$id = nil
$include_pkgs = nil
$exclude_pkgs = nil
options = OptionParser::new do |opts|
  opts.banner = "Usage: ./generate-tasks-json [options]"
  opts.separator ""
  opts.separator "Options:"
  opts.on("-m", "--mode MODE", "Special mode") { |m| $modes << m }
  opts.on("-d", "--dist DISTRIBUTION", "Distribution to test (default: unstable)") { |t| $dist = t }
  opts.on("-i", "--id TEXT", "Additional identifier for logfiles") { |t| $id = t }
  opts.on("", "--include FILE", "List of packages to include") { |t| $include_pkgs = IO::read(t).split }
  opts.on("", "--exclude FILE", "List of packages to exclude") { |t| $exclude_pkgs = IO::read(t).split }
end
options.parse!(ARGV)

pkgsgz = open("http://localhost:9999/debian/dists/#{$dist}/main/binary-amd64/Packages.gz")
gz = Zlib::GzipReader.new(pkgsgz)
pkgs = gz.read.split(/\n/).
       grep(/^Package: /).
       map { |l| l.split[1] }.
       reject { |e| EXCLUDED_PKGS.include?(e[0]) }

tasks = []
pkgs.each do |e|
  t = {}
  t['type'] = 'instest'
  t['package'] = e
  t['dist'] = $dist
  t['esttime'] = nil
  if $id
    t['logfile'] = "/tmp/#{t['package']}_#{t['dist']}_#{$id}.log"
  else
    t['logfile'] = "/tmp/#{t['package']}_#{t['dist']}.log"
  end
  next if $include_pkgs and not $include_pkgs.include?(t['package'])
  next if $exclude_pkgs and $exclude_pkgs.include?(t['package'])
  t['modes'] = $modes
  tasks << t
end

puts JSON::pretty_generate(tasks)
